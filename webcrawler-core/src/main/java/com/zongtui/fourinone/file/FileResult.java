package com.zongtui.fourinone.file;

public class FileResult<E> extends Result {
	public FileResult(){
		super();
	}
	
	public FileResult(boolean ready)
	{
		super(ready);
	}
	
	public static FileResult getExceptionResult(){
		FileResult fr = new FileResult(false);
		fr.setReady(FileResult.EXCEPTION);
		return fr;
	}
}