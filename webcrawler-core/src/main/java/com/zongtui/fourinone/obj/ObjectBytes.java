package com.zongtui.fourinone.obj;

import com.zongtui.fourinone.utils.log.LogUtil;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ObjectBytes {
    static private char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".toCharArray();
    static private byte[] codes = new byte[256];

    static {
        for (int i = 0; i < 256; i++) codes[i] = -1;
        for (int i = 'A'; i <= 'Z'; i++) codes[i] = (byte) (i - 'A');
        for (int i = 'a'; i <= 'z'; i++) codes[i] = (byte) (26 + i - 'a');
        for (int i = '0'; i <= '9'; i++) codes[i] = (byte) (52 + i - '0');
        codes['+'] = 62;
        codes['/'] = 63;
    }

    public static byte[] toBytes(Object o) {
        byte[] gbt = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oout = new ObjectOutputStream(baos);//gzout
            oout.writeObject(o);
            byte[] objgbt = baos.toByteArray();
            //System.out.println(objgbt.length);
            baos.close();
            oout.close();

            gbt = getByteFromIs(new ByteArrayInputStream(objgbt), true);
        } catch (Exception e) {
            LogUtil.info("[ObjectBytes]", "[toBytes]", "[Error Exception:]", e);
        }
        return gbt;
    }

    public static byte[] getByteFromIs(InputStream gis, boolean GZIPFlag) {
        byte[] gbt = null;
        try {
            BufferedInputStream is = new BufferedInputStream(gis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            OutputStream gzout = GZIPFlag ? (new GZIPOutputStream(baos)) : baos;
            BufferedOutputStream os = new BufferedOutputStream(gzout);
            int ch;
            try {
                while ((ch = is.read()) != -1)
                    os.write(ch);
            } catch (EOFException eofex) {/*for end the stream*/}

            is.close();
            os.close();
            gbt = baos.toByteArray();
        } catch (Exception e) {
            LogUtil.info("[ObjectBytes]", "[getByteFromIs]", "[Error Exception:]", e);
        }
        return gbt;
    }

    public static Object toObject(byte[] bts) {
        ByteArrayOutputStream sbos = new ByteArrayOutputStream();
        setByteToOs(bts, sbos, true);
        byte[] unzipbts = sbos.toByteArray();

        Object obj = null;
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(unzipbts);
            ObjectInputStream ois = new ObjectInputStream(bais);
            obj = ois.readObject();
            bais.close();
            ois.close();
        } catch (Exception e) {
            LogUtil.info("[ObjectBytes]", "[toObject]", "[Error Exception:]", e);
        }
        return obj;
    }

    public static void setByteToOs(byte[] sbt, OutputStream sos, boolean GZIPFlag) {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(sbt);
            InputStream gzin = GZIPFlag ? (new GZIPInputStream(bais)) : bais;
            BufferedInputStream is = new BufferedInputStream(gzin);
            BufferedOutputStream os = new BufferedOutputStream(sos);

            int ch;
            try {
                while ((ch = is.read()) != -1)
                    os.write(ch);
            } catch (EOFException eofex) {/*for end the stream*/}

            is.close();
            os.close();
        } catch (Exception e) {
            LogUtil.info("[ObjectBytes]", "[setByteToOs]", "[Error Exception:]", e);
        }
    }

    public static String encode(String data) {
        return new String(encode(data.getBytes()));
    }

    public static String encodeReplace(String data) {
        return encode(data, "\u003D", "\u005F");
    }

    public static String encode(String data, String ostr, String rplstr) {
        return encode(data).replaceAll(ostr, rplstr);
    }

    public static String encodeurl(String data) {
        return getUrlString(encode(data));
    }

    public static char[] encode(byte[] data) {
        char[] out = new char[((data.length + 2) / 3) * 4];
        for (int i = 0, index = 0; i < data.length; i += 3, index += 4) {
            boolean quad = false, trip = false;
            int val = (0xFF & (int) data[i]);
            val <<= 8;
            if ((i + 1) < data.length) {
                val |= (0xFF & (int) data[i + 1]);
                trip = true;
            }
            val <<= 8;
            if ((i + 2) < data.length) {
                val |= (0xFF & (int) data[i + 2]);
                quad = true;
            }
            out[index + 3] = alphabet[(quad ? (val & 0x3F) : 64)];
            val >>= 6;
            out[index + 2] = alphabet[(trip ? (val & 0x3F) : 64)];
            val >>= 6;
            out[index + 1] = alphabet[val & 0x3F];
            val >>= 6;
            out[index] = alphabet[val & 0x3F];
        }
        return out;
    }

    public static String decode(String data) {
        return new String(decode(data.toCharArray()));
    }

    public static String decodeReplace(String data) {
        return decode(data, "\u005F", "\u003D");
    }

    public static String decode(String data, String ostr, String rplstr) {
        return decode(data.replaceAll(ostr, rplstr));
    }

    public static String decodeurl(String data) {
        return decode(getViewUrlString(data));
    }

    public static byte[] decode(char[] data) {
        int len = ((data.length + 3) / 4) * 3;
        if (data.length > 0 && data[data.length - 1] == '=') --len;
        if (data.length > 1 && data[data.length - 2] == '=') --len;
        byte[] out = new byte[len];
        int shift = 0, accum = 0, index = 0;
        for (char aData : data) {
            int value = codes[aData & 0xFF];
            if (value >= 0) {
                accum <<= 6;
                shift += 6;
                accum |= value;
                if (shift >= 8) {
                    shift -= 8;
                    out[index++] = (byte) ((accum >> shift) & 0xff);
                }
            }
        }
        if (index != out.length) throw new Error("miscalculated data length!");
        return out;
    }

    public static byte[] getCharSequence(byte[] bts) {
        for (int i = 0; i < bts.length; i++)
            bts[i] = (byte) ~bts[i];
        return bts;
    }

    static String getUrlString(String viewStr) {
        String urlStr = "";
        if (viewStr != null)
            urlStr = URLEncoder.encode(viewStr);
        return urlStr;
    }

    static String getViewUrlString(String urlStr) {
        String viewStr = "";
        if (urlStr != null)
            viewStr = URLDecoder.decode(urlStr);//aaa = java.net.URLDecoder.decode(aaa,"UTF-8");
        return viewStr;
    }

    public static String getUtf8UrlString(String viewStr) {
        String urlStr = "";
        try {
            if (viewStr != null)
                urlStr = URLEncoder.encode(viewStr, "UTF-8");//if default,can no langCode
        } catch (Exception e) {
            LogUtil.fine(e);
        }
        return urlStr;
    }

    public static String getViewUtf8UrlString(String urlStr) {
        String viewStr = "";
        try {
            if (urlStr != null)
                viewStr = URLDecoder.decode(urlStr, "UTF-8");//new String(urlStr.getBytes("8859_1"),"UTF-8");//if default,can no langCode
        } catch (Exception e) {
            LogUtil.fine(e);
        }
        return viewStr;
    }

    public static String getEscape(String fttpstr) {
        if (fttpstr == null)
            return null;

        char[] chararr = fttpstr.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char aChararr : chararr) {
            switch (aChararr) {
                case '%':
                    sb.append("%25");
                    break;
                case ' ':
                    sb.append("%20");
                    break;
                case '[':
                    sb.append("%5B");
                    break;
                case ']':
                    sb.append("%5D");
                    break;
                case '\\':
                    sb.append("%5C");
                    break;
                case '{':
                    sb.append("%7B");
                    break;
                case '}':
                    sb.append("%7D");
                    break;
                case '<':
                    sb.append("%3C");
                    break;
                case '>':
                    sb.append("%3E");
                    break;
                case '|':
                    sb.append("%7C");
                    break;
                case '^':
                    sb.append("%5E");
                    break;
                case '\"':
                    sb.append("%22");
                    break;
                default:
                    sb.append(aChararr);
            }
        }
        return sb.toString();
    }
}