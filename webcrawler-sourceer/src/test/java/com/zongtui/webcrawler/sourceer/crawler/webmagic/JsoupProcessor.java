package com.zongtui.webcrawler.sourceer.crawler.webmagic;

import java.util.List;

import javax.management.JMException;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.scheduler.QueueScheduler;
import us.codecraft.webmagic.scheduler.component.BloomFilterDuplicateRemover;

/**
 * @author code4crafter@gmail.com <br>
 */
public class JsoupProcessor implements PageProcessor {

    private Site site = Site.me().setDomain("http://www.open-open.com").addStartUrl("http://www.open-open.com/jsoup/");

    @Override
    public void process(Page page) {
    	//http://www.open-open.com/jsoup/parse-document-from-string.htm
        List<String> links = page.getHtml().links().regex("http://www\\.open-open\\.com/jsoup/[\\w+[\\-]*]*.htm").all();
        page.addTargetRequests(links);
        page.putField("title", page.getHtml().xpath("//div[@class='recipe']/h1/text()").toString());
        page.putField("content", page.getHtml().xpath("//div[@class='recipe']/tidyText()").toString());
       // page.putField("tags",page.getHtml().xpath("//div[@class='BlogTags']/a/text()").all());
    }

    @Override
    public Site getSite() {
        return site;

    }

    public static void main(String[] args) throws JMException {
        Spider spider = Spider.create(new JsoupProcessor()).setScheduler(new QueueScheduler().setDuplicateRemover(new BloomFilterDuplicateRemover(2000)));
        //SpiderMonitor.instance().register(spider);
        spider.run();
    }
}
