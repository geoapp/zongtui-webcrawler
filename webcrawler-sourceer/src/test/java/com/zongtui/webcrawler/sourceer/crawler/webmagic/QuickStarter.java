/**
 * Project Name:webcrawler-sourceer
 * File Name:QuickStarter.java
 * Package Name:com.zongtui.webcrawler.sourceer.crawler
 * Date:2015-4-29下午6:03:34
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */

package com.zongtui.webcrawler.sourceer.crawler.webmagic;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.model.OOSpider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.pipeline.MultiPagePipeline;

import com.zongtui.webcrawler.sourceer.crawler.webmagic.model.IteyeBlog;
import com.zongtui.webcrawler.sourceer.crawler.webmagic.model.News163;

/**
 * ClassName: QuickStarter <br/>
 * Function: QuickStarter. <br/>
 * date: 2015-4-29 下午6:03:34 <br/>
 *
 * @author feng
 * @version 
 * @since JDK 1.7
 */
public class QuickStarter {
	private static Map<String, Class> clazzMap;

    private static Map<String, String> urlMap;

    private static void init(){
        clazzMap = new LinkedHashMap<String, Class>();
        clazzMap.put("1", OschinaBlog.class);
        clazzMap.put("2", IteyeBlog.class);
        clazzMap.put("3", News163.class);
        urlMap = new LinkedHashMap<String, String>();
        urlMap.put("1", "http://my.oschina.net/flashsword/blog");
        urlMap.put("2", "http://flashsword20.iteye.com/");
        urlMap.put("3", "http://news.163.com/");
    }

    public static void main(String[] args) {
        init();
        String key = null;
        key = readKey(key);
        System.out.println("The demo started and will last 20 seconds...");
        //Start spider
        OOSpider.create(Site.me().addStartUrl(urlMap.get(key)), clazzMap.get(key)).pipeline(new MultiPagePipeline()).pipeline(new ConsolePipeline()).runAsync();

        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("The demo stopped!");
        System.out.println("To more usage, try to customize your own Spider!");
        System.exit(0);
    }

    private static String readKey(String key) {
        Scanner stdin = new Scanner(System.in);
        System.out.println("Choose a Spider demo:");
        for (Map.Entry<String, Class> classEntry : clazzMap.entrySet()) {
            System.out.println(classEntry.getKey()+"\t" + classEntry.getValue() + "\t" + urlMap.get(classEntry.getKey()));
        }
        while (key == null) {
            key = new String(stdin.nextLine());
            if (clazzMap.get(key) == null) {
                System.out.println("Invalid choice!");
                key = null;
            }
        }
        return key;
    }
}

