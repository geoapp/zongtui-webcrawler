<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/line7">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/line7" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
            var option = {
            	    title : {
            	        text: '双数值轴折线',
            	        subtext: '纯属虚构'
            	    },
            	    tooltip : {
            	        trigger: 'axis',
            	        axisPointer:{
            	            show: true,
            	            type : 'cross',
            	            lineStyle: {
            	                type : 'dashed',
            	                width : 1
            	            }
            	        },
            	        formatter : function (params) {
            	            return params.seriesName + ' : [ '
            	                   + params.value[0] + ', ' 
            	                   + params.value[1] + ' ]';
            	        }
            	    },
            	    legend: {
            	        data:['数据1','数据2']
            	    },
            	    toolbox: {
            	        show : true,
            	        feature : {
            	            mark : {show: true},
            	            dataZoom : {show: true},
            	            dataView : {show: true, readOnly: false},
            	            magicType : {show: true, type: ['line', 'bar']},
            	            restore : {show: true},
            	            saveAsImage : {show: true}
            	        }
            	    },
            	    calculable : true,
            	    xAxis : [
            	        {
            	            type: 'value'
            	        }
            	    ],
            	    yAxis : [
            	        {
            	            type: 'value',
            	            axisLine: {
            	                lineStyle: {
            	                    color: '#dc143c'
            	                }
            	            }
            	        }
            	    ],
            	    series : [
            	        {
            	            name:'数据1',
            	            type:'line',
            	            data:[
            	                [1.5, 10], [5, 7], [8, 8], [12, 6], [11, 12], [16, 9], [14, 6], [17, 4], [19, 9]
            	            ],
            	            markPoint : {
            	                data : [
            	                    // 纵轴，默认
            	                    {type : 'max', name: '最大值',symbol: 'emptyCircle', itemStyle:{normal:{color:'#dc143c',label:{position:'top'}}}},
            	                    {type : 'min', name: '最小值',symbol: 'emptyCircle', itemStyle:{normal:{color:'#dc143c',label:{position:'bottom'}}}},
            	                    // 横轴
            	                    {type : 'max', name: '最大值', valueIndex: 0, symbol: 'emptyCircle', itemStyle:{normal:{color:'#1e90ff',label:{position:'right'}}}},
            	                    {type : 'min', name: '最小值', valueIndex: 0, symbol: 'emptyCircle', itemStyle:{normal:{color:'#1e90ff',label:{position:'left'}}}}
            	                ]
            	            },
            	            markLine : {
            	                data : [
            	                    // 纵轴，默认
            	                    {type : 'max', name: '最大值', itemStyle:{normal:{color:'#dc143c'}}},
            	                    {type : 'min', name: '最小值', itemStyle:{normal:{color:'#dc143c'}}},
            	                    {type : 'average', name : '平均值', itemStyle:{normal:{color:'#dc143c'}}},
            	                    // 横轴
            	                    {type : 'max', name: '最大值', valueIndex: 0, itemStyle:{normal:{color:'#1e90ff'}}},
            	                    {type : 'min', name: '最小值', valueIndex: 0, itemStyle:{normal:{color:'#1e90ff'}}},
            	                    {type : 'average', name : '平均值', valueIndex: 0, itemStyle:{normal:{color:'#1e90ff'}}}
            	                ]
            	            }
            	        },
            	        {
            	            name:'数据2',
            	            type:'line',
            	            data:[
            	                [1, 2], [2, 3], [4, 2], [7, 5], [11, 2], [18, 3]
            	            ]
            	        }
            	    ]
            	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>