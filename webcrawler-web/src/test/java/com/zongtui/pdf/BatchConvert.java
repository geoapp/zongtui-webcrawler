package com.zongtui.pdf;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.zongtui.links.ListLinks;

/**
 * ClassName: BatchConvert <br/>
 * Function: 对获得的url多个路径批量转换为pdf. <br/>
 * date: 2015年5月4日 下午7:16:24 <br/>
 *
 * @author liuyuqi
 * @version
 * @since JDK 1.7
 */

public class BatchConvert {
	@Test
	public void coventToPDF() throws IOException {
		String url = "http://www.open-open.com/jsoup/";
		String savePath = null;
		String wkhtmltopdfPath = "D:\\Program Files\\wkhtmltopdf";

		ListLinks listLinks = new ListLinks();
		List<String> URLs = listLinks.getLinks(url);
		WkhtmltopdfTest wkhtmltopdfTest = new WkhtmltopdfTest();
		for (int i = 0; i < URLs.size(); i++) {
			savePath = "D:/aa" + i + ".pdf";
			wkhtmltopdfTest.saveAsPDFDefault(URLs.get(i), savePath,
					wkhtmltopdfPath);
		}
		System.out.println("finish");
	}
}
